.. _CARS: http://www.inogs.it/en/content/cartography-and-remote-sensing 

.. _pyproj:  http://code.google.com/p/pyproj/

.. _numpy:  http://numpy.scipy.org

.. _PySide:  http://www.pyside.org

.. _PyQt4:  http://www.riverbankcomputing.co.uk/software/pyqt/download

.. _PROJ.4:  http://trac.osgeo.org/proj/

.. _Whitaker: http://www.esrl.noaa.gov/psd/people/jeffrey.s.whitaker/

CCQW Python Package Documentation
=================================

Foreword
********
CCQW has been developed by two programmers working in the CARS_
(CArtography and Remote Sensing) group of the IRI Research Section at
`OGS - Istituto Nazionale di Oceanografia e di
Geofisica Sperimentale <http://www.inogs.it>`_.

Python is their favourite programming language since 2006.

The authors:

**Nicola Creati, Roberto Vidmar**

.. image:: images/creati.jpg
   :height: 134 px
   :width:  100 px 
.. image:: images/vidmar.jpg
   :height: 134 px
   :width:  100 px 

What is CCQW?
*************
**CCQW is a package** with modules (widgets) for:
  * display/edit geographic degrees
    (:class:`ccqw.degreesWidget.DegreesWidget`)
    in the following formats:

      * degrees
      * degrees, minutes
      * degrees, minutes, seconds

    with converstions to and from any of them

  * display/edit geographic coordinates
    (:class:`ccqw.coordWidget.CoordWidget`)
    with and without geoid correction
  * display/edit plane coordinates
    (:class:`ccqw.projectedWidget.ProjectedWidget`)
    with and without geoid correction
  * convert coordinates
    (:class:`ccqw.coordDialog.CoordDialog`)
    by re-projection and optional datum shift
  * apply or remove geoid correction
  * convert ASCII files of coordinates with an interactive tool to choose
    columns of data
  * select to/from Datum and Projection
    (:class:`ccqw.projDialog.ProjDialog`)
    from any Datum and Projection defined in the PROJ.4_ library

It relies on PySide_ (or PyQt4_, see :class:`ccqw.qtCompat`)
for the gui and on PROJ.4_ for coordinate projections.

Credits
*******
This package was possible thanks to Jeff Whitaker_ who released pyproj_,
the Python interface to PROJ.4_ library, and helped us
many times in our work.

.. note::
   The package depends on either PySide_ or PyQt4_, pyproj_ and numpy_ and has
   ben tested with Python 2.6.6, PySyde 1.1.0, PyQt4 4.8.1, pyproj 1.9.0 and
   numpy 1.3.0.

More
****
.. toctree::
  :maxdepth: 2

  How does the widget look like? <snapshots>

  CCQW Package Reference: <ccqw>

.. warning::
   This code has been tested *only* on Linux (Ubuntu 10.10, 12.04) but
   should work
   also on Mac and Windows (Xp and greater).

.. warning::
   This is work in progress!


Indices and Tables
==================
* :ref:`genindex`
* :ref:`modindex`
