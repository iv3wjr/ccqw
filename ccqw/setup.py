# setup.py

# Roberto Vidmar 20120720

# Run
# python setup.py py2exe
#
# to build a windows standalone.
# Will create a "dist" folder containing all files.
# setup.py is THIS file.

from distutils.core import setup
import py2exe
import os.path
import glob

# CCQW files
filelist = [
           "..\i18n_it_IT.qm",  # Italian Localization
           "..\license.txt",    # License info file
           "ccqw.ico",          # CCQW icon
           "ccqw_preinfo.txt",  # Information file before install
           "ccqw_postinfo.txt", # Information file after install
           ]
inst_filedir = "." # Where to put files
inst_filelist = []

foldername = "files" 
for f in filelist:
  inst_filelist.append(os.path.join(foldername, f)) # Files to get

dll_excludes = ["MSVCP90.dll"]

setup(
  windows=["coordDialog.py"],
  data_files=[
              (inst_filedir, inst_filelist),
			  ("data", ["data\epsg", ]),  #EPSG database
             ],
  options={ "py2exe": {"dist_dir" : "windows_standalone",
                       "dll_excludes" : dll_excludes
                      }
          }
  )
