""" A module to convert all image files in a directory into a resource file
    for later compilation with pyrcc4

    :Author: 2009-2011 Nicola Creati

    :Copyright: 2011-2012
                Nicola Creati <ncreati@inogs.it>
                Roberto Vidmar <rvidmar@inogs.it>

    :License: MIT/X11 License (see :download:`license.txt
                               <../../license.txt>`)
"""

import os
import glob


if __name__ == '__main__':
  IMAGES_PATH = 'images'
  RESOURCE = 'icons.qrc'
  RESOURCE_PY = 'icons_rc.py'

  pixmaps = glob.glob(os.path.join(IMAGES_PATH, '*.png'))

  rFile = open(RESOURCE, 'w')
  rFile.write('<!DOCTYPE RCC><RCC version="1.0">\n')
  rFile.write('<qresource>\n')

  for pix in pixmaps:
      rFile.write('<file alias="%s">%s</file>\n' % (os.path.basename(pix), pix))

  rFile.write('</qresource>\n')
  rFile.write('</RCC>\n')
  rFile.close()

  command = 'pyrcc4 %s -o %s' % (RESOURCE, RESOURCE_PY)
  print 'Command:', command
  os.system(command)
  print '>>>>>>>>>>>>>>> Conversion ended <<<<<<<<<<<<<<<<<<<<'
