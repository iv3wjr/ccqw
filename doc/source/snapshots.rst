How does the widget look like?
==============================

The main Coordinates Dialog window:

.. image:: images/CoordinatesDialog.png
   :scale: 100 % 

Choose Datum and Projection:

.. image:: images/DatumAndProjections.png
   :scale: 100 % 

User Defined PROJ.4 strings defining Datums and Projections can be saved for
future reference right clicking on the **User Defined Coordinate System** row.
Definitions can be renamed by double clicking on it or removed with the
**<DEL>** key:

.. image:: images/UserDefinedCoordinateSystem.png
   :scale: 100 % 

The fields wizard that helps to choose columns from ASCII files:

.. image:: images/FieldsWizard.png
   :scale: 100 % 

After selection of all mandatory fields the **Done** button is enabled. ASCII
files may have a Header that can be skipped selecting the corrisponding lines
in the "file preview" area. Blank lines as well as commented lines (beginning
with the '#' character) are skipped automatically:

.. image:: images/FieldsWizard-1.png
   :scale: 100 % 

While selecting fields, only remaining fields are selectable:

.. image:: images/SelectDegrees.png
   :scale: 100 % 

.. image:: images/SelectMinutes.png
   :scale: 100 % 

.. image:: images/SelectSeconds.png
   :scale: 100 % 

Field selection can be saved for future use:

.. image:: images/SaveThisFormatToFile.png
   :scale: 100 % 

