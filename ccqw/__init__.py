""" A Python package made of several modules to display and convert
    coordinates ineractively with a GUI.
    Library PROJ.4 is used for coordinate conversion.

    :Author:
      - 20111227-20120111 Roberto Vidmar

    :Revision:  $Revision: 56 $
                $Date: 2012-11-12 15:14:43 +0000 (Mon, 12 Nov 2012) $

    :Copyright: 2011-2012
                Nicola Creati <ncreati@inogs.it>
                Roberto Vidmar <rvidmar@inogs.it>

    :License: MIT/X11 License (see :download:`license.txt
                               <../../license.txt>`)
"""
