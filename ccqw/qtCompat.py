# -*- coding: utf-8 -*-
""" The main initialization of our PySide/PyQt4 Package.

  .. warning:: this module tries to
     import PySide if available, otherwise defaults to PyQt4 for the GUI.
     To change this behaviour set *_TRY_PYSIDE* to False.


  :Author:
    - 2009-2011 Nicola Creati
    - 2009-2011 Roberto Vidmar

  :Revision:  $Revision: 56 $
              $Date: 2012-11-12 15:14:43 +0000 (Mon, 12 Nov 2012) $

  :Copyright: 2011-2012
              Nicola Creati <ncreati@inogs.it>
              Roberto Vidmar <rvidmar@inogs.it>

  :License: MIT/X11 License (see :download:`license.txt
                               <../../license.txt>`)
"""

from __future__ import with_statement
from __future__ import division

_TRY_PYSIDE = True

try:
  if not _TRY_PYSIDE:
    raise ImportError()
  import PySide.QtCore as _QtCore
  import PySide.QtGui as _QtGui
  QtCore = _QtCore
  QtGui = _QtGui
  USES_PYSIDE = True
except ImportError:
  import sip
  sip.setapi('QString', 2)
  sip.setapi('QVariant', 2)
  import PyQt4.QtCore as _QtCore
  import PyQt4.QtGui as _QtGui
  QtCore = _QtCore
  QtGui = _QtGui
  USES_PYSIDE = False

def _pyside_import_module(moduleName):
  """ The import for PySide
  """
  pyside = __import__('PySide', globals(), locals(), [moduleName], -1)
  return getattr(pyside, moduleName)

def _pyqt4_import_module(moduleName):
  """ The import for PyQt4
  """
  pyside = __import__('PyQt4', globals(), locals(), [moduleName], -1)
  return getattr(pyside, moduleName)

if USES_PYSIDE:
  import_module = _pyside_import_module

  Signal = QtCore.Signal
  Slot = QtCore.Slot
  Property = QtCore.Property

  def getOpenFileName(*args, **kargs):
    """ Wrap to PySide QtGui.QFileDialog.getOpenFileName
    """
    pn, selectedFilter = QtGui.QFileDialog.getOpenFileName(*args, **kargs)
    return pn

  def getOpenFileNames(*args, **kargs):
    """ Wrap to PySide QtGui.QFileDialog.getOpenFileNames
    """
    pn, selectedFilter = QtGui.QFileDialog.getOpenFileNames(*args, **kargs)
    return pn

  def getSaveFileName(*args, **kargs):
    """ Wrap to PySide QtGui.QFileDialog.getSaveFileName
    """
    pn, selectedFilter = QtGui.QFileDialog.getSaveFileName(*args, **kargs)
    return pn
else:
  import_module = _pyqt4_import_module

  Signal = QtCore.pyqtSignal
  Slot = QtCore.pyqtSlot
  Property = QtCore.pyqtProperty

  def getOpenFileName(*args, **kargs):
    """ Wrap to PyQt4 QtGui.QFileDialog.getOpenFileName
    """
    return QtGui.QFileDialog.getOpenFileName(*args, **kargs)

  def getOpenFileNames(*args, **kargs):
    """ Wrap to PyQt4 QtGui.QFileDialog.getOpenFileNames
    """
    return QtGui.QFileDialog.getOpenFileNames(*args, **kargs)

  def getSaveFileName(*args, **kargs):
    """ Wrap to PyQt4 QtGui.QFileDialog.getSaveFileName
    """
    return QtGui.QFileDialog.getSaveFileName(*args, **kargs)

  QtCore.pyqtRemoveInputHook()

Qt = QtCore.Qt
