<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>Convert Button</name>
    <message>
        <location filename="coordDialog.py" line="212"/>
        <source>Convert File</source>
        <translation>Converti il File</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="214"/>
        <source>Click here to convert input File</source>
        <translation>Fare click qui per convertire il file di ingresso</translation>
    </message>
</context>
<context>
    <name>CoordWidget</name>
    <message>
        <location filename="coordDialog.py" line="188"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="189"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="190"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="191"/>
        <source>Input File</source>
        <translation>File di ingresso</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="192"/>
        <source>Output File</source>
        <translation>File di uscita</translation>
    </message>
</context>
<context>
    <name>Done message</name>
    <message>
        <location filename="coordDialog.py" line="433"/>
        <source>File Conversion</source>
        <translation>Conversione del file</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="442"/>
        <source>Ok</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="438"/>
        <source>File has been convertd to %s
Output format is &apos;%s&apos;.</source>
        <translation>Il file è stato convertito in %s
con il formato &apos;%s&apos;.</translation>
    </message>
</context>
<context>
    <name>Error message</name>
    <message>
        <location filename="coordDialog.py" line="352"/>
        <source>ERROR reading input file.....</source>
        <translation>Si è verificato un errore leggendo il file in ingresso...</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="356"/>
        <source>ERROR!
Cannot understand input file:
Maybe format or projection is wrong?</source>
        <translation>ERRORE!
Il file di ingresso non è stato
interpretato correttamente:
Forse è sbagliato il formato o la proiezione?</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="360"/>
        <source>Sorry</source>
        <translation>Peccato</translation>
    </message>
</context>
<context>
    <name>FieldSelectionDialog</name>
    <message>
        <location filename="fieldsWizard.py" line="194"/>
        <source>This column range is:</source>
        <translation>L&apos;estensione della colonna è:</translation>
    </message>
</context>
<context>
    <name>FilePicker</name>
    <message>
        <location filename="coordWidget.py" line="163"/>
        <source>XYZ files (*.xyz);;All Files (*)</source>
        <translation>File di tipo XYZ (*.xyz);;Tutti i File (*)</translation>
    </message>
</context>
<context>
    <name>HeightWidget</name>
    <message>
        <location filename="coordDialog.py" line="370"/>
        <source>Heights are orthometric</source>
        <translation>Le quote sono ortometriche</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="373"/>
        <source>Heights are ellipsoidic</source>
        <translation>Le quote sono ellissoidiche</translation>
    </message>
    <message>
        <location filename="heightWidget.py" line="79"/>
        <source>Enter here ellipsoidical height in meters</source>
        <translation>Inserire qui la quota ellissoidica in metri</translation>
    </message>
    <message>
        <location filename="heightWidget.py" line="81"/>
        <source>Enter here orthometric height in meters</source>
        <translation>Inserire qui la quota ortometrica in metri</translation>
    </message>
</context>
<context>
    <name>Ok to convert</name>
    <message>
        <location filename="coordDialog.py" line="302"/>
        <source>Convert File</source>
        <translation>Conversione di File</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="305"/>
        <source>Ok to convert
&apos;%s&apos; to
&apos;%s&apos;
with format &apos;%s&apos; ?</source>
        <translation>Pronto a convertire
&apos;%s&apos; in
&apos;%s&apos;
con il formato &apos;%s&apos; ? </translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="coordDialog.py" line="428"/>
        <source>Conversion in progress...</source>
        <translation>Conversione in corso...</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="429"/>
        <source>Converting...</source>
        <translation>Sto convertendo...</translation>
    </message>
</context>
<context>
    <name>ProjDialog</name>
    <message>
        <location filename="projDialog.py" line="118"/>
        <source>Datum and Projections</source>
        <translation>Datum e Proiezioni</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="135"/>
        <source>The current Datum/Projection PROJ.4 string</source>
        <translation>La stringa PROJ.4 che definisce il Datum e la Proiezione corrente</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="144"/>
        <source>Current Projection:  </source>
        <translation>Proiezione corrente: </translation>
    </message>
    <message>
        <location filename="projDialog.py" line="146"/>
        <source>The Datum/Projection in use now</source>
        <translation>Il Datum/la proiezione attualmente in uso</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="155"/>
        <source>Search for PROJ.4 (sub)string or EPSG code:</source>
        <translation>Ricerca di una (sotto)stringa PROJ.4 oppure un codice EPSG:</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="158"/>
        <source>You can search for any text through all
known Coordinate Reference Systems
and EPSG codes</source>
        <translation>E&apos; possibile ricercare qualsiasi testo
tra tutti i Sistemi di Coordinate ed i codici EPSG</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="257"/>
        <source>1 of 1</source>
        <translation>E&apos; l&apos;unico</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="259"/>
        <source>Move to %s of %s</source>
        <translation>Vai al n. %s su un totale di %s</translation>
    </message>
</context>
<context>
    <name>ProjModel</name>
    <message>
        <location filename="projMVC.py" line="396"/>
        <source>Coordinate Reference System</source>
        <translation>Sistema di Riferimeto delle Coordinate</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="397"/>
        <source>EPSG</source>
        <translation>EPSG</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="398"/>
        <source>Proj.4</source>
        <translation>Proj.4</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="431"/>
        <source>New Projection</source>
        <translation>Nuova Proiezione</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="594"/>
        <source>.
Right click on this item to add
your custom Coordinate System.
Double click to rename it
Press &lt;DEL&gt; key to delete it.</source>
        <translation>.
Premere il tasto destro del mouse
su questo elemento per aggiungere
il vostro consueto Sistema di Cordinate.
Fare doppio click per rinominarlo
Premere il tasto &lt;Canc&gt; per cancellarlo.</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="597"/>
        <source>The &quot;%s&quot; group%s</source>
        <translation>Il gruppo &quot;%s&quot;%s</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="604"/>
        <source>User defined projection &quot;%s&quot;%s</source>
        <translation>La proiezione definita dall&apos;utente &quot;%s&quot;%s</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="607"/>
        <source>EPSG projection &quot;%s&quot;</source>
        <translation>La proiezione EPSG è &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="613"/>
        <source>Original EPSG code was %s%s</source>
        <translation>Il codice EPSG originale era %s%s</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="616"/>
        <source>EPSG code is %s%s</source>
        <translation>Il codice EPSG è %s%s</translation>
    </message>
</context>
<context>
    <name>ProjectedWidget</name>
    <message>
        <location filename="projectedWidget.py" line="68"/>
        <source>Enter here %s in meters.decimals</source>
        <translation>Inserire qui la %s in metri.decimali</translation>
    </message>
    <message>
        <location filename="projectedWidget.py" line="71"/>
        <source>Enter here meters.decimals</source>
        <translation>Inserire qui metri.decimali</translation>
    </message>
</context>
<context>
    <name>SaveEPSGDialog</name>
    <message>
        <location filename="projMVC.py" line="1055"/>
        <source>Save current EPSG projection among user&apos;s defined?</source>
        <translation>Vuoi salvare la proiezione EPSG corrente tra quelle consuete?</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="projDialog.py" line="45"/>
        <source>Enter here the string to search for.
This string will be searched through
all known coordinate reference system</source>
        <translation>Inserire qui la stringa da ricercare.
Questa stringa verrà cercata in tutto
l&apos;archivio dei sistemi di riferimento
di coorinate conosciuti</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="53"/>
        <source>New Search:</source>
        <translation>Nuova Ricerca:</translation>
    </message>
    <message>
        <location filename="projDialog.py" line="51"/>
        <source>Click here to search for the string to the right
among all known coordinate reference system</source>
        <translation>Fare click qui per iniziare la ricerca della
stringa qui a destra tra tutti i sistemi di
riferimento di coordinate conosciuti</translation>
    </message>
</context>
<context>
    <name>UnimplementedMessage</name>
    <message>
        <location filename="coordDialog.py" line="43"/>
        <source>Unimplemented</source>
        <translation>Non implementato</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="47"/>
        <source>This conversion has not been
implemented yet.</source>
        <translation>Questa conversione non è
ancora stata implementata.</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="51"/>
        <source>Ok</source>
        <translation>Continua</translation>
    </message>
</context>
<context>
    <name>UserDefinedDialog</name>
    <message>
        <location filename="projMVC.py" line="987"/>
        <source>Enter here a valid PROJ.4 string
The string is validated while you type.
If the string is NOT valid you cannot save it.
WARNING... a valid PROJ.4 string
does NOT mean that it is correct!</source>
        <translation>Inserire qui una stringa PROJ.4 valida.
La stringa viene validata durante la
composizione. Se la stringa non è
valida è impossibile salvarla.
ATTENZIONE... una stringa valida
secondo la sintassi PROJ.4 potrebbe
comunque essere errata!</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="990"/>
        <source>Enter a valid PROJ.4 string below:</source>
        <translation>Inserire una stringa valida secondo
la sintassi PROJ.4 qui sotto:</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="995"/>
        <source>Below you can define your own Datum/Projection:
Use the standard PROJ.4 syntax plus the keywords
+datumShift= and +geoidgrids= to define custom
Datum Shift and Geoid (.gtx) files.
</source>
        <translation>Qui sotto è possibile definire una proprio Datum e/o
una propria Proiezione: Usate la sintassi PROJ.4
assieme alle parole chiave +datumShift= e
+geoidgrids= per definire i propri file che contengono i
cambiamenti di Datum ed i grigliati con il Geoide
(file di tipo .gtx).</translation>
    </message>
</context>
<context>
    <name>coordDialog</name>
    <message>
        <location filename="coordDialog.py" line="176"/>
        <source>The name of this point</source>
        <translation>Il nome di questo punto</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="168"/>
        <source>Proj.4 Coordinate Converter - Rev. %s</source>
        <translation>Convertitore di Coordinate Proj.4 - Rev. %s</translation>
    </message>
</context>
<context>
    <name>datum tooltip</name>
    <message>
        <location filename="coordWidget.py" line="220"/>
        <source>Change current Datum / Projection</source>
        <translation>Modifica il Datum o la Proiezione corrente</translation>
    </message>
</context>
<context>
    <name>datumGroup</name>
    <message>
        <location filename="coordWidget.py" line="300"/>
        <source>Datum and Projection:</source>
        <translation>Datum e Proiezione:</translation>
    </message>
</context>
<context>
    <name>datumMbox</name>
    <message>
        <location filename="coordWidget.py" line="719"/>
        <source>Datum Shift Error</source>
        <translation>Errore nel cambiamento di Datum</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="721"/>
        <source>&apos;%s&apos; is not a valid Datum Shift file.</source>
        <translation>&apos;%s&apos; non è un file valido per il cambiamento di Datum.</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="709"/>
        <source>IO Error</source>
        <translation>Errore di lettura</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="712"/>
        <source>Cannot load datum shift file &apos;%s&apos;.
The reason is:
%s.</source>
        <translation>Non è stato possibile caricare il file
con il cambiamento di Datumm &apos;%s&apos;.
Il motivo è:
%s.</translation>
    </message>
</context>
<context>
    <name>degreesWidget</name>
    <message>
        <location filename="degreesWidget.py" line="363"/>
        <source>Latitude</source>
        <translation>Latitudine</translation>
    </message>
    <message>
        <location filename="degreesWidget.py" line="367"/>
        <source>Longitude</source>
        <translation>Longitudine</translation>
    </message>
    <message>
        <location filename="degreesWidget.py" line="372"/>
        <source>Enter here %s in
degrees.decimals</source>
        <translation>Inserisci qui la %s in
gradi.decimali</translation>
    </message>
    <message>
        <location filename="degreesWidget.py" line="374"/>
        <source>Enter here %s in
degrees, minutes.decimals</source>
        <translation>Inserisci qui la %s in
gradi, minuti.decimali</translation>
    </message>
    <message>
        <location filename="degreesWidget.py" line="376"/>
        <source>Enter here %s in
degrees, minutes, seconds.decimals</source>
        <translation>Inserisci qui la %s in
gradi, minuti, secondi.decimali</translation>
    </message>
</context>
<context>
    <name>degs</name>
    <message>
        <location filename="coordWidget.py" line="370"/>
        <source>D.DD</source>
        <translation>G.GG</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="371"/>
        <source>D M.MM</source>
        <translation>G M.MM</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="371"/>
        <source>D M S.SS</source>
        <translation>G M S.SS</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="375"/>
        <source>Change angular representation among
degrees.decimals,
degrees, minutes.decimals
and degrees, minutes, seconds.decimals</source>
        <translation>Scegli la rappresentazione angolare tra
gradi.decimali,
gradi, minuti.decimali,
e gradi, minuti, secondi.decimali</translation>
    </message>
</context>
<context>
    <name>easting</name>
    <message>
        <location filename="coordWidget.py" line="157"/>
        <source>Easting</source>
        <translation>Est</translation>
    </message>
</context>
<context>
    <name>fieldsSelector</name>
    <message>
        <location filename="fieldsWizard.py" line="384"/>
        <source>Range Overlap Error</source>
        <translation>Errore di sovrapposizione di intervallo</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="386"/>
        <source>This selection overlaps another one.
Make another selection.</source>
        <translation>Questa selezione si sovrappone ad
una precedente. Fate un&apos;altra selezione.</translation>
    </message>
</context>
<context>
    <name>fieldsWizard</name>
    <message>
        <location filename="fieldsWizard.py" line="513"/>
        <source>Format files (*%s);;All Files (*%s*)</source>
        <translation>File di formato (*%s);;Tutti i File (*%s*)</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="589"/>
        <source>Load From File</source>
        <translation>Caricare da File</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="594"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="599"/>
        <source>Reset</source>
        <translation>Azzerare</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="646"/>
        <source>Header defined</source>
        <translation>Testata definita</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="650"/>
        <source>Input file has a header.
Header has been defined.</source>
        <translation>Il file di ingresso ha una testata.
La testata è stata definita.</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="653"/>
        <source>This file
has a Header</source>
        <translation>Questo file
ha una testata</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="659"/>
        <source>Check this box if input
file has a header.
Then select header lines
and right click to save.</source>
        <translation>Spuntare questa casella
se il file di ingresso ha una testata.
In seguito seleziore le righe della
testata e fare click con il tasto destro
del mouse per salvare.</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="740"/>
        <source>Save this format to file</source>
        <translation>Salvare questo formato su file</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="764"/>
        <source>Load format from file</source>
        <translation>Caricare il formato da file</translation>
    </message>
</context>
<context>
    <name>file</name>
    <message>
        <location filename="coordWidget.py" line="378"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="381"/>
        <source>
Or select File for file conversion</source>
        <translation>Oppure seleziona File per la conversione di file</translation>
    </message>
</context>
<context>
    <name>fileBrowserWidget</name>
    <message>
        <location filename="fieldsWizard.py" line="275"/>
        <source>This is a line of the file</source>
        <translation>Questa è una riga del file</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="292"/>
        <source>Header Error</source>
        <translation>Errore nella testata</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="295"/>
        <source>Header must start from line one,
not from line %d.
Please make another selection.</source>
        <translation>La testata deve partire dalla prima
riga, non dalla riga %d.
Selezionare nuovamante la testata.</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="306"/>
        <source>This is the selected line</source>
        <translation>Questa è la riga selezionata</translation>
    </message>
    <message>
        <location filename="fieldsWizard.py" line="320"/>
        <source>This line belongs to the Header</source>
        <translation>Questa riga appartiene alla testata</translation>
    </message>
</context>
<context>
    <name>filePicker</name>
    <message>
        <location filename="filePicker.py" line="171"/>
        <source>Open file</source>
        <translation>Apri il file</translation>
    </message>
    <message>
        <location filename="filePicker.py" line="172"/>
        <source>Click to select the input file</source>
        <translation>Fare click per selezionare il file di ingresso</translation>
    </message>
    <message>
        <location filename="filePicker.py" line="175"/>
        <source>Enter here the name of the file to convert</source>
        <translation>Inserire qui il nome del file da convertire</translation>
    </message>
    <message>
        <location filename="filePicker.py" line="182"/>
        <source>Save to</source>
        <translation>Salvare con il nome</translation>
    </message>
    <message>
        <location filename="filePicker.py" line="184"/>
        <source>Click to select the output file</source>
        <translation>Fare click per selezioare il file di uscita</translation>
    </message>
    <message>
        <location filename="filePicker.py" line="188"/>
        <source>Enter here the name of the output file</source>
        <translation>Inserire qui il nome del file di uscita</translation>
    </message>
</context>
<context>
    <name>geoidMbox</name>
    <message>
        <location filename="coordWidget.py" line="744"/>
        <source>Geoid Grid Error</source>
        <translation>Errore nel grigliato del Geoide</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="746"/>
        <source>&apos;%s&apos; is not a valid Geoid Grid file.</source>
        <translation>Il file &apos;%s&apos; non contiene una definizione valida di grigliato del Geoide.</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="734"/>
        <source>IO Error</source>
        <translation>Errore di lettura</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="737"/>
        <source>Cannot load geoid grid file &apos;%s&apos;.
The reason is:
%s.</source>
        <translation>Impossibile caricare il grigliato del geoide dal file &apos;%s&apos;.
Il motivo è:
%s.</translation>
    </message>
</context>
<context>
    <name>hLabelTooltip</name>
    <message>
        <location filename="coordWidget.py" line="137"/>
        <source>Height</source>
        <translation>Quota</translation>
    </message>
</context>
<context>
    <name>hcombo</name>
    <message>
        <location filename="coordWidget.py" line="277"/>
        <source>ell</source>
        <translation>ell</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="277"/>
        <source>ortho</source>
        <translation>orto</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="281"/>
        <source>Change height representation among
ellipsoidical and orthometric height in meters</source>
        <translation>Scegli quale quota visualizzare tra
ellissoidica ed ortometrica in metri</translation>
    </message>
</context>
<context>
    <name>latwidget</name>
    <message>
        <location filename="coordWidget.py" line="174"/>
        <source>Latitude</source>
        <translation>Latitudine</translation>
    </message>
</context>
<context>
    <name>lonwidget</name>
    <message>
        <location filename="coordWidget.py" line="176"/>
        <source>Longitude</source>
        <translation>Longitudine</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="coordDialog.py" line="633"/>
        <source>optional height to add to get orthometric height (default: &apos;%(default)s&apos;)</source>
        <translation>quota opzionale da aggiungere per avere la quota ortometrica (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="637"/>
        <source>optional name for the dialog (default: &apos;%(default)s&apos;)</source>
        <translation>nome opzionale per il dialogo (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="641"/>
        <source>optional upper widget representation [0..2] (default: &apos;%(default)s&apos;)</source>
        <translation>rappresentazione opzionale per il widget superiore [0..2] (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="648"/>
        <source>degrees, minutes, seconds characters (default: &apos;%(default)s&apos;)</source>
        <translation>caratteri per i gradi, i primi ed i secondi (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="623"/>
        <source>point coordinates (default: &apos;%(default)s&apos;)</source>
        <translation>coordinate del punto (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="626"/>
        <source>upper Proj.4 string (default: &apos;%(default)s&apos;)</source>
        <translation>stringa Proj.4 superiore (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="629"/>
        <source>lower Proj.4 string (default: &apos;%(default)s&apos;)</source>
        <translation>stringa Proj.4 inferiore (default: &apos;%(default)s&apos;)</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="645"/>
        <source>optional lower widget representation [0..2] (default: &apos;%(default)s&apos;)</source>
        <translation>optional lower widget representation [0..2] (default: &apos;%(default)s&apos;)</translation>
    </message>
</context>
<context>
    <name>noDatumShift</name>
    <message>
        <location filename="coordDialog.py" line="68"/>
        <source>Ok</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="60"/>
        <source>No Datum Shift</source>
        <translation>Cambiamento di Datum non definito</translation>
    </message>
</context>
<context>
    <name>northing</name>
    <message>
        <location filename="coordWidget.py" line="158"/>
        <source>Northing</source>
        <translation>Nord</translation>
    </message>
</context>
<context>
    <name>outfile</name>
    <message>
        <location filename="coordDialog.py" line="419"/>
        <source># Converted on %s:
</source>
        <translation># Convertito il %s:</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="420"/>
        <source># From Proj.4: &apos;%s&apos; - %s
</source>
        <translation># Da Proj.4: &apos;%s&apos; - %s</translation>
    </message>
    <message>
        <location filename="coordDialog.py" line="422"/>
        <source>#   To Proj.4: &apos;%s&apos; - %s
</source>
        <translation># A Proj.4: &apos;%s&apos; - %s</translation>
    </message>
</context>
<context>
    <name>plane</name>
    <message>
        <location filename="coordWidget.py" line="370"/>
        <source>Easting, Northing in meters</source>
        <translation>Est, Nord in metri</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="368"/>
        <source>M.MM</source>
        <translation>M.MM</translation>
    </message>
</context>
<context>
    <name>projMVC</name>
    <message>
        <location filename="projMVC.py" line="32"/>
        <source>User defined Coordinate System</source>
        <translation>Sistema di Coordinate definito dall&apos;utente</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="33"/>
        <source>lightsteelblue</source>
        <translation>lightsteelblue</translation>
    </message>
    <message>
        <location filename="projMVC.py" line="34"/>
        <source>lightgray</source>
        <translation>lightgray</translation>
    </message>
</context>
<context>
    <name>projMbox</name>
    <message>
        <location filename="coordWidget.py" line="757"/>
        <source>Invalid PROJ.4 string</source>
        <translation>La stringa non è una stringa valida secondo la sintassi PROJ.4</translation>
    </message>
    <message>
        <location filename="coordWidget.py" line="760"/>
        <source>&apos;%s&apos; is an invalid PROJ.4 string.
The reason is:
%s.</source>
        <translation>la stringa &apos;%s&apos; non è valida secondo la sintassi PROJ.4.
Il motivo è:
%s.</translation>
    </message>
</context>
<context>
    <name>projTooltip</name>
    <message>
        <location filename="coordWidget.py" line="233"/>
        <source>This is the current Datum / Projection PROJ.4 string</source>
        <translation>Questa è la stringa che rappresenta il Datum / la Proiezione corrente nella sintassi PROJ.4</translation>
    </message>
</context>
</TS>
