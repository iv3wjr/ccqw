@echo off
echo Roberto Vidmar, 20120720
echo.  
echo Build CCQW Standalone for Windows
echo.  
pause
echo Remove old copy
del /f /s /q  windows_standalone\*.*

echo  Build new Windows standalone
echo Copy epsg database to data directory
copy c:\Python27\Lib\site-packages\pyproj\data\epsg data
python setup.py py2exe
