# A PySide / PyQt based widget to convert coordinates interactively using PROJ.4. #

The dialog supports conversion of points or files to different Datum / Projection and has a column select wizard to select interactively coordinates from ASCII files.

The package depends on sip, PySyde/PyQt4, pyproj, numpy.
If your platform is Ubuntu you can find below instructions on how to install what's
needed.

-----------------------------
Install **PySide**

The easiest way to get **PySide** installed on your Ubuntu system is this (from http://qt-project.org/wiki/PySide_Binaries_Linux):


```
#!

sudo add-apt-repository ppa:pyside
sudo apt-get update
sudo apt-get install python-pyside
```
---------------------
Install **numpy**


```
#!

sudo apt-get install python-numpy
```
-------------------
Install **pyproj** >= 1.9.0


```
#!

sudo apt-get install python-dev
wget http://pyproj.googlecode.com/files/pyproj-1.9.0.tar.gz
tar zxvf pyproj-1.9.0.tar.gz
cd pyproj-1.9.0
sudo python setup.py install
```
--------------------------------------
## Documentation ##

Documentation about ccqw can be found [here](https://bitbucket.org/iv3wjr/ccqw/src/master/doc/html/index.html).

## Get the code ##

To test the package check out a read-only working copy anonymously over *https*:


```
#!

git clone https://bitbucket.org/iv3wjr/ccqw ccqw-read-only
```
then


```
#!

cd ccqw-read-only
python coordDialog.py
```

### Who do I talk to? ###

* [Roberto Vidmar](http://www.inogs.it//users/roberto-vidmar)
* [Nicola Creati](http://www.inogs.it//users/nicola-creati)